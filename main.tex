\documentclass[10pt]{beamer}

\usetheme[progressbar=frametitle,numbering=fraction]{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{numprint}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}


\title{Machine-Learning Based Side-Channel Analysis Applied to Embedded Systems Security}
\subtitle{ARPE Report 2019-2020}
% \date{\today}
\date{}
\author{Arthur Grisel-Davy}
\institute{ENS Paris-Saclay x University of Waterloo}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}

\maketitle

\begin{frame}[fragile]{Host University and Lab}
\begin{figure}
  \includegraphics[width=0.6\textwidth]{images/uwaterloo_logo.jpg}
\end{figure}
\begin{figure}
   \includegraphics[height=0.4\paperheight]{images/waterloo_map.png}
   \hfill
   \includegraphics[height=0.4\paperheight]{images/sebastian.jpg}
   \caption{Sebastian Fischmeister, Head of the Embedded Software Group at the University of Waterloo.}
\end{figure}
\end{frame}

\begin{frame}{Table of contents}
  \vspace{0.5cm}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents%[hideallsubsections]
\end{frame}


\section{Introduction}
\subsection{Side-Channels}

\begin{frame}[fragile]{Side-Channels}
  \begin{itemize}[<+- | alert@+>]
    \Large
    \item Physical emissions from the system:
      \begin{itemize}[<+- | alert@+>]
        \Large
        \item Electromagnetic fields.
        \item Sound (audible or ultrasound)
        \item Power consumption information
      \end{itemize}
    \item Involuntary emissions
    \item Representative of the activities of the system.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Exploitation of Side-Channels}
\begin{columns}
  \begin{column}{0.6\textwidth}
  \onslide<2->{
  Attacks:
    \begin{itemize}
      \item 1996: Paul C. Kosher: Timing attacks on RSA keys
      \item Sound attacks on keyboards, printers, 3D printers
      \item Electromagnetic attacks on SoC, wireless communication devices
    \end{itemize}
  }
  \onslide<3->{
  \vspace{1cm}
  Defense:
    \begin{itemize}
      \item Defense against side-channel attacks: noise
      \item Leverage of side-channels to detect attacks
    \end{itemize}
  }
  \end{column}
  \begin{column}{0.4\textwidth}
  \centering
  \onslide<2->{
    \includegraphics[width=0.7\textwidth]{images/attacks}
  }
  \vspace{2cm}
  \onslide<3->{
    \includegraphics[width=0.9\textwidth]{images/defense}
  }
  \end{column}
\end{columns}
\end{frame}

\subsection{Attacks}

\begin{frame}[fragile]{Attacks}
\begin{itemize}[<+- | alert@+>]
\Large
    \item Firmware manipulation.
    \begin{itemize}[<+- | alert@+>]
    \Large
      \item Main firmware of the system.
      \item Firmware of the components.
    \end{itemize}
    \item SSH login attempts.
    \item Unauthorized hardware installation.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Projects}

\begin{columns}
  \begin{column}{0.5\textwidth}
  \onslide<2->{
  \begin{center}
  
  \includegraphics[height=0.15\textwidth]{images/switch}\\
  \vspace{1cm}
  {\LARGE EET Project}
  \end{center}
  \begin{itemize}
    \item Protection of network equipment against:
    \begin{itemize}
      \item Firmware manipulation
      \item SSH log tempering
      \item Hardware manipulation
    \end{itemize}
  \end{itemize}
  }
  \end{column}

  \begin{column}{0.5\textwidth}
  \only<3>{
  \begin{center}
  
  \includegraphics[height=0.25\textwidth]{images/computer}\\
  \vspace{1cm}
  {\LARGE xPSU Project}
  \end{center}
  \begin{itemize}
    \item Protection of personal computers against:
    \begin{itemize}
      \item Motherboard firmware manipulation
      \item Hard-drive firmware manipulation
    \end{itemize}
  \end{itemize}
  }
  \end{column}
\end{columns}
\end{frame}

\section{EET Project}

\begin{frame}[fragile]{Pipeline Overview}
\begin{center}
\begin{figure}
\includegraphics[width=\textwidth]{images/eet-overview.pdf}
\caption{Overview of the pipeline of the EET project.}
\end{figure}
\end{center}
\end{frame}

\subsection{Firmware manipulation}
\begin{frame}[fragile]{Firmware Manipulation - {\small Mainly conducted by Goksen Guler}}

\begin{columns}
\begin{column}{0.45\textwidth}
\begin{itemize}[<+- | alert@+>]
  \item Training set of 1000 traces of 120 seconds from 10 versions.
  \item Traces captured at 1MSPS downsampled to 1SPS.
  \item Sliding median filter.
  \item RFC and SVM models on AC/DC, time/frequency data.
\end{itemize}
\end{column}

\begin{column}{0.55\textwidth}
\begin{figure}
\includegraphics[width=\textwidth]{images/Firmware_Comparison_TD_direct}
\caption{Power consumption comparison for two firmware versions.}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Results}
\begin{center}
\begin{table}
\begin{tabularx}{\textwidth}{XXXXX}
    \toprule
    \textbf{Model} & \textbf{Macro Precision} & \textbf{Macro Recall} & \textbf{Macro F1 Score} & \textbf{Accuracy} \tabularnewline
    \midrule
    & \multicolumn{3}{>{\hsize=\dimexpr3\hsize+3\tabcolsep+\arrayrulewidth\relax}c}{\textbf{Time Domain – DC Data}} & \tabularnewline
    \midrule
    RFC & \numprint[\%]{100} & \numprint[\%]{100} & \numprint[\%]{100}  & \textcolor{red}{\numprint[\%]{100}} \tabularnewline
    SVM & \numprint[\%]{97.0}  & \numprint[\%]{97.4} & \numprint[\%]{96.8} & \numprint[\%]{99.3}\tabularnewline
    \midrule
    & \multicolumn{3}{>{\hsize=\dimexpr3\hsize+3\tabcolsep+\arrayrulewidth\relax}c}{\textbf{Time Domain – AC Data}} & \tabularnewline
    \midrule
    RFC & \numprint[\%]{90.0} & \numprint[\%]{93.7} & \numprint[\%]{87.4}  & \numprint[\%]{98.9} \tabularnewline
    SVM & \numprint[\%]{80.7}  & \numprint[\%]{75.1} & \numprint[\%]{75.8} & \numprint[\%]{95.5} \tabularnewline
    \midrule
    & \multicolumn{3}{>{\hsize=\dimexpr3\hsize+3\tabcolsep+\arrayrulewidth\relax}c}{\textbf{Frequency Domain – DC Data}} & \tabularnewline
    \midrule
    RFC & \numprint[\%]{97.0} & \numprint[\%]{96.5} & \numprint[\%]{97.6} & \numprint[\%]{99.8} \tabularnewline
    SVM & \numprint[\%]{95.5} & \numprint[\%]{96.5} & \numprint[\%]{95.3} & \numprint[\%]{96.0} \tabularnewline
    \bottomrule
\end{tabularx}
\caption{Results for firmware detection.}
\end{table}
\end{center}
\end{frame}

\subsection{SSH Log Tempering}

\begin{frame}[fragile]{SSH Log Tempering - {\small Mainly conducted by Shikhar Sakhuja}}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{itemize}[<+- | alert@+>]
  \item Training set of 4095 samples from 85 traces.
  \item Traces captured at 1MSPS downsampled to 1KSPS.
  \item RFC, SVM and 1DCNN models on DC, time/frequency data.
\end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
\centering
\begin{figure}
\includegraphics[width=0.9\textwidth]{images/time_domain_ssh.pdf}
\includegraphics[width=0.9\textwidth]{images/time_domain_ssh_labels.pdf}
\caption{Power consumption and labels for SSH capture.}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Results}
\begin{center}
\begin{table}
\begin{tabularx}{\textwidth}{XXXXXXX}
    \toprule
    \textbf{Model} & \textbf{Precision} & \textbf{Recall} & \textbf{F1 Score} & \textbf{Accuracy} & \textbf{FPR} & \textbf{FNR} \tabularnewline
    \midrule
    & \multicolumn{5}{>{\hsize=\dimexpr5\hsize+5\tabcolsep+\arrayrulewidth\relax}c}{\textbf{Time Domain}} & \tabularnewline
    \midrule
    RFC & \numprint[\%]{95} & \numprint[\%]{97} & \numprint[\%]{95} & \numprint[\%]{97} & \numprint[\%]{0.6} & \numprint[\%]{14} \tabularnewline
    SVM & \numprint[\%]{95} & \numprint[\%]{97} & \numprint[\%]{96} & \textcolor{red}{\numprint[\%]{98}} & \numprint[\%]{0.8} & \numprint[\%]{8} \tabularnewline
    1D~CNN & \numprint[\%]{94} & \numprint[\%]{93} & \numprint[\%]{93} & \numprint[\%]{96} & \numprint[\%]{2} & \numprint[\%]{9} \tabularnewline
    \midrule
    & \multicolumn{5}{>{\hsize=\dimexpr5\hsize+5\tabcolsep+\arrayrulewidth\relax}c}{\textbf{Frequency Domain}} & \tabularnewline
    \midrule
    RFC & \numprint[\%]{89} & \numprint[\%]{67} & \numprint[\%]{72} & 
    \numprint[\%]{88} & 
    \numprint[\%]{12} & 
    \numprint[\%]{8} \tabularnewline
    SVM & -- & -- & -- & -- & -- & --  \tabularnewline
    1D~CNN & 
    \numprint[\%]{90} & \numprint[\%]{90} & \numprint[\%]{90} & \numprint[\%]{94} & 
    \numprint[\%]{3} & 
    \numprint[\%]{17} \tabularnewline
    \midrule
    & \multicolumn{5}{>{\hsize=\dimexpr5\hsize+5\tabcolsep+\arrayrulewidth\relax}c}{\textbf{Time + Frequency Domain}} & \tabularnewline
    \midrule
    1D~CNN & \numprint[\%]{89} & \numprint[\%]{95} & \numprint[\%]{92} & \numprint[\%]{95} & \numprint[\%]{1} & \numprint[\%]{20} \tabularnewline
    \bottomrule
\end{tabularx}
\caption{Results for SSH attemps detection.}
\end{table}
\end{center}
\end{frame}

\subsection{Hardware Manipulation}

\begin{frame}[fragile]{Hardware Manipulation}
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{images/eet-hardware-installation.pdf}
\caption{Impact of installation of modules on DC power consumption.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Hardware Manipulation}
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{images/eet-hardware-ac-overlap.png}
\caption{Impact of installation of modules on DC power consumption.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Hardware Manipulation}
\begin{itemize}[<+- | alert@+>]
\Large
  \item DC training set of 1380 samples from 138 traces.
  \item AC training set of 4320 samples from 138 traces.
  \item KNN and SVM models on AC/DC, time data.
\end{itemize}
\end{frame}


\begin{frame}[fragile]{Results}
\begin{center}
\begin{table}
    \begin{tabularx}{\columnwidth}{XXXXX}
        \toprule
        \textbf{Input data} & \textbf{Model} & \textbf{Accuracy} & \textbf{Recall}\tabularnewline
        \midrule
         DC & SVM & \numprint[\%]{100} & \numprint[\%]{100}\tabularnewline
         DC & 1-NN & \numprint[\%]{100} & \numprint[\%]{100}\tabularnewline
         \midrule
         AC & SVM & \numprint[\%]{99.5} & \numprint[\%]{99.45}\tabularnewline
        \bottomrule
    \end{tabularx}
    \caption{Results for hardware installation detection.}
\end{table}
\end{center}
\end{frame}

\subsection{Conclusion of the EET Project}

\begin{frame}[fragile]{Conclusion of the EET Project}
Achievements:
\begin{itemize}
  \item Illustrate the potential of the side-channel analysis to IDS.
  \begin{itemize}
    \item Various attack types.
    \item Reduced training set.
    \item Simple and robust ML techniques.
  \end{itemize}
  \item Creation of a powerful and versatile capture pipeline.
\end{itemize}

\onslide<2->{
Issues:
\begin{itemize}
    \item Sample size is only one machine.
    \item Pipeline oriented towards capture and not detection.
\end{itemize}
}
\end{frame}

\section{xPSU Project}

\begin{frame}[fragile]{Motherboard and Hard-Drive Firmware Manipulation}
\begin{figure}
\includegraphics[width=0.8\textwidth]{images/motherboard-confmat.pdf}
\caption{Confusion matrix for motherboard's firmware classification.}
\end{figure}
\end{frame}


\begin{frame}[fragile]{Motherboard and Hard-Drive Firmware Manipulation}
\begin{figure}
\includegraphics[width=0.8\textwidth]{images/harddrive-confmat.pdf}
\caption{Confusion matrix for hard-drives's firmware classification.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Project Overview}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/xpsu-overview.pdf}
\caption{Overview of the xPSU project.}
\end{figure}
\end{frame}

\subsection{xPSU Hardware}

\begin{frame}[fragile]{xPSU Hardware}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/overview.JPG}
\caption{Inside of the xPSU.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{xPSU Hardware}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/overview_legend.JPG}
\caption{Inside of the xPSU.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{xPSU Hardware}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/footprint.JPG}
\caption{Form factor of the xPSU.}
\end{figure}
\end{frame}


\subsection{Detection Process}

\begin{frame}[fragile]{Detection Process}
\Large
Detection Process:
\begin{itemize}[<+- | alert@+>]
\Large
  \item Controler script and xPSU client are started.
  \item Machine starts, bootup sequence is captured.
  \item Capture is done, trace is sent to controler and analysed.
  \item Results are sent back to the client and further actions (quarantine, re-capture) may be taken.
\end{itemize}
\end{frame}

\subsection{Conclusion of the xPSU Project}

\begin{frame}[fragile]{Conclusion of the xPSU Project}
Achievements:
\begin{itemize}
  \item Illustrate the possibility to embedd EET in a PSU.
  \item Broaden the application range to hard-drive firmwares
\end{itemize}
Future Work:
\begin{itemize}
    \item Improvement of the form factor and tapping channels.
    \item Outlier detection algorithm.
    \item Ransomware and quarantine.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{}
\begin{center}
Thank you for your attention.
\end{center}
\end{frame}

\appendix


\begin{frame}[fragile]{Ransomware Detection}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/wannacry_idle_1.png}
\caption{DC power consumption during wannacry attack and idle usage.}
\end{figure}
\end{frame}

\begin{frame}[fragile]{Ransomware Detection}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{images/wannacry_stress.png}
\caption{DC power consumption during wannacry attack and stressed usage.}
\end{figure}
\end{frame}

\end{document}